import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-post-list-item-component',
  templateUrl: './post-list-item-component.component.html',
  styleUrls: ['./post-list-item-component.component.scss']
})
export class PostListItemComponentComponent implements OnInit {

	@Input( ) inputPost ;
		
  constructor() {
	  }

  ngOnInit() {
  }
  
  likeIt(){
	  console.log("I Like It");
	  this.inputPost.loveIts=1;
  }
  notLikeIt(){
	  console.log("notLikeIt");
	  this.inputPost.loveIts=0;
  }
  

}
